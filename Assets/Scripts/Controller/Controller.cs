using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class Controller : MonoBehaviour
{
    public static Action<Controller> OnNewController;
    public static Action<Controller> OnControllerLost;
    public static Action OnPause;

    private static List<Controller> list = new List<Controller>();

    public enum Device { Controller, Keyboard};

    private Device deviceType;
    public Device DeviceType => deviceType;
    private PlayerInput playerInput;

    //Value
    [SerializeField] private Vector2 movement = Vector2.zero;
    public Vector2 Movement => movement;
    [SerializeField] private Vector2 view = Vector2.zero;
    public Vector2 View => view;

    private bool grab = false;
    public bool Grab => grab;

    public Action OnInput;

    private void Start()
    {
        list.Add(this);
        playerInput = GetComponent<PlayerInput>();
        deviceType = playerInput.devices[0].name == "Keyboard" ? Device.Keyboard : Device.Controller;

        OnNewController?.Invoke(this);
    }

    private void OnDestroy()
    {
        list.Remove(this);
    }

    public void Pause()
    {
        OnPause?.Invoke();
    }

    public void SetMoveX(InputAction.CallbackContext ctx)
    {
        movement.x = ctx.ReadValue<float>();
    }

    public void SetMoveY(InputAction.CallbackContext ctx)
    {
        movement.y = ctx.ReadValue<float>();
    }

    public void SetViewX(InputAction.CallbackContext ctx)
    {
        view.x = ctx.ReadValue<float>();
    }

    public void SetViewY(InputAction.CallbackContext ctx)
    {
        view.y = ctx.ReadValue<float>();
    }

    public void SetGrab(InputAction.CallbackContext ctx)
    {
        if (hardcodeWaiting) return;
        StartCoroutine(EnModeHardcode());

        if (ctx.ReadValue<Single>() == 1) OnInput?.Invoke();
    }

    private bool hardcodeWaiting = false;
    private IEnumerator EnModeHardcode()
    {
        hardcodeWaiting = true;
        yield return null;
        hardcodeWaiting = false;
    }

    public void OnDisconnect(PlayerInput pInput)
    {
        Debug.Log("disconnect");
        if (playerInput == pInput)
        {
            OnControllerLost(this);
            Destroy(gameObject);
        }
    }
}
