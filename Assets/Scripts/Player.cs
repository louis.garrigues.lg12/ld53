using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMFunds.Audio;
using TMFunds.Patterns;

public class Player : MonoBehaviour
{
	private static List<Player> list = new List<Player>();
	private Player enemy;

	[SerializeField] private PlayerParameters _Parameters;
	[SerializeField] public Controller controller;
	[SerializeField] private Transform visual;
	[SerializeField] private Animator animator;
	[SerializeField] private AnimationCurve speedToAnim;
	private Rigidbody rb;
	private StateMachine loop = new StateMachine();

	[Header("Hands")]
	[SerializeField] private BoxGrabber boxGrabber;
	[SerializeField] private BoxCollider boxCollisionSimulator;
	[SerializeField] private Transform[] hands;
	[SerializeField] private Transform[] virtualHands;
	private bool handsOnVirtual = true;

	private bool isGrabbing = false;

	private Box _Box;

	[Header("Juiciness")]
	[SerializeField] private ParticleSystem collisionPrt;
	[SerializeField] private MeshRenderer aimArrow;
	private Material arrowMat;
	private Transform arrowParent;

	private float virtualRotation = 0;
	private Vector3 VirtualVector => new Vector3(Mathf.Cos(virtualRotation * Mathf.Deg2Rad), 0, Mathf.Sin(virtualRotation * Mathf.Deg2Rad));
	private Quaternion VirtualQuaternion => Quaternion.AngleAxis(virtualRotation, Vector3.down);

	private void Start()
    {
		list.Add(this);

		arrowMat = aimArrow.material;
		arrowParent = aimArrow.transform.parent;

		rb = GetComponent<Rigidbody>();
		loop.Set(MainLoop);
    }

	public void SetController(Controller pController)
    {
		controller = pController;
		controller.OnInput += Grabbing;
		boxGrabber.SetControllerType(controller.DeviceType == Controller.Device.Keyboard ? 1 : 0);
	}

    private void OnDestroy()
    {
		if(controller != null) controller.OnInput -= Grabbing;
		list.Remove(this);
	}

    void Update()
	{
		loop.Call();
	}

	private void MainLoop()
    {
		Move();
		PlaceHands();
	}

	private void CatchedLoop()
    {
		Move();
		PlaceHands();
		PlaceArrow();
	}

	private float hittedCount = 0;
	private void HittedLoop()
    {
		PlaceHands();
		hittedCount += Time.deltaTime;
		if(hittedCount > 1f)
        {
			loop.Set(MainLoop);
			animator.SetBool("Hitted", false);
			boxGrabber.StartCheck();
		}
	}

	private void Grabbing()
    {
      if (isGrabbing)
        {
			//Ungrab
			StartCoroutine(HideArrow());
			loop.Set(MainLoop);
			handsOnVirtual = true;
			isGrabbing = false;
			_Box.SetModeReleased((arrowParent.right + Vector3.up * 0.3f) * 15f);
			boxCollisionSimulator.enabled = false;
			_Box = null;

			boxGrabber.StartCheck();
		}
        else
        {
			//Grab

			_Box = boxGrabber.Catch();
			if (_Box != null) Grab();
		}
    }

	private void Grab()
    {
		boxGrabber.EndCheck();
		boxGrabber.Emptying();

		handsOnVirtual = false;
		isGrabbing = true;
		Vector3 lBoxPos = transform.position + VirtualVector * 1.15f + Vector3.down * 0.2f;
		_Box.SetModeCatch(visual, lBoxPos, VirtualQuaternion, this);
		boxCollisionSimulator.enabled = true;

		SearchEnemy();
		StartCoroutine(ShowArrow());
		loop.Set(CatchedLoop);

		//placeHands
		hands[0].position = lBoxPos + visual.forward * 0.5f * _Box.transform.localScale.x;
		hands[1].position = lBoxPos + visual.forward * -0.5f * _Box.transform.localScale.x;
	}
	
	private void Move()
    {
		//Variables
		Vector2 lMovement = Vector2.ClampMagnitude(controller != null ? controller.Movement : Vector2.zero, 1f);
		float lMagnitude = lMovement.magnitude;
		
		//Logic
		virtualRotation = Mathf.LerpAngle(virtualRotation, Mathf.Rad2Deg * Mathf.Atan2(lMovement.y, lMovement.x), Time.deltaTime * lMagnitude * _Parameters.RotationSpeed);
		rb.velocity = VirtualVector * lMagnitude * _Parameters.Speed;

		//Visual
		transform.rotation = VirtualQuaternion;
		animator.SetFloat("Speed", speedToAnim.Evaluate(lMagnitude));
    }

	private void PlaceHands()
    {
        if (handsOnVirtual)
        {
			for(int i = hands.Length - 1; i >=0; i--)
            {
				hands[i].position = virtualHands[i].position;
            }
        }
    }

	private void SearchEnemy()
    {
		for(int i = list.Count - 1; i >= 0; i--)
        {
			if (list[i] != this) enemy = list[i];
        }
    }

	private void PlaceArrow()
    {
		Vector3 lDirTo = enemy.transform.position - transform.position;
		lDirTo.Normalize();
		arrowParent.transform.rotation = Quaternion.Slerp(
			Quaternion.LookRotation(transform.forward, Vector3.up),
			Quaternion.LookRotation(lDirTo, Vector3.up) * Quaternion.AngleAxis(90, Vector3.down),
			Mathf.Max(0, Vector3.Dot(transform.right, lDirTo.normalized)));
    }

	public void Hit(Box pBox, float pMagnitude, Vector3 pImpact)
    {
		//Particles
		collisionPrt.transform.position = pImpact;
		collisionPrt.transform.LookAt(pBox.transform.position);
		collisionPrt.Play();

		//Logic
		loop.Set(HittedLoop);
		handsOnVirtual = true;
		hittedCount = 0;
		animator.SetBool("Hitted", true);

		//Box behave
		if(_Box != null)
        {
			_Box.SetModeReleased(Vector3.up * 5f);
			boxCollisionSimulator.enabled = false;
			StartCoroutine(HideArrow());
			loop.Set(MainLoop);
		}

		//Orientation
		Vector2 l2DDirection = (new Vector2(transform.position.x, transform.position.z) - new Vector2(pBox.transform.position.x, pBox.transform.position.z)).normalized;
		virtualRotation = Mathf.Atan2(l2DDirection.y, l2DDirection.x) * Mathf.Rad2Deg;
		transform.rotation = VirtualQuaternion;

		//Physic
		l2DDirection *= Mathf.Max(pMagnitude - 5f, 0) * 1.5f;
		rb.velocity = new Vector3(l2DDirection.x, 0, l2DDirection.y);
    }

	private float arrowTime = 0.2f;

	private IEnumerator ShowArrow()
    {
		float lCount = 0f;
		while (lCount < 1f)
        {
			arrowMat.SetFloat("_Ratio", lCount);
			lCount += Time.deltaTime / arrowTime;
			yield return null;
        }
		arrowMat.SetFloat("_Ratio", 1f);
	}

	private IEnumerator HideArrow()
    {
		float lCount = 0f;
		while (lCount < 1f)
		{
			arrowMat.SetFloat("_Ratio", 1 - lCount);
			lCount += Time.deltaTime / arrowTime;
			yield return null;
		}
		arrowMat.SetFloat("_Ratio", 0f);
	}

	public void LoseBox()
    {
		StartCoroutine(HideArrow());
		loop.Set(MainLoop);
		handsOnVirtual = true;
		isGrabbing = false;
		boxCollisionSimulator.enabled = false;
		_Box = null;
    }
}