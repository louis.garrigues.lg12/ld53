using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TMFunds.Math
{
    public static class Geo3
    {
        public static Vector3 Orbital2Cart(Vector3 pivot, float yaw, float pitch, float radius = 1f)
        {
            float cosP = Mathf.Cos(pitch);
            return new Vector3(pivot.x + cosP * Mathf.Cos(yaw) * radius,
                pivot.y + Mathf.Sin(pitch) * radius,
                pivot.z + cosP * Mathf.Sin(yaw) * radius
                );
        }

        /*
        public static float AngleLerp05(float a, float b, float t)
        {
            a %= 180;
            b %= 180;
            float b2 = b + (a > b ? 180 : -180);
            return Mathf.Abs(a - b) < Mathf.Abs(a - b2) ? Mathf.Lerp(a, b)
        }
        */

        public static float RangeModulo(float value, float offset, float modulo)
        {
            return ((value - offset) % modulo) + offset;
        }
    }
}


