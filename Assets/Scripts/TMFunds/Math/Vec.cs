using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TMFunds.Math
{
    public static class Vec
    {
        public static Vector3 New(float value)
        {
            return new Vector3(value, value, value);
        }

        public static Vector3 New(Quaternion value)
        {
            return value * Vector3.one;
        }

        public static Vector3 AddDimension(Vector2 value)
        {
            return new Vector3(value.x, value.y, 0);
        }

        public static Vector3 AddDimension(Vector2 value, float newValue)
        {
            return new Vector3(value.x, value.y, newValue);
        }

        public static Vector2 RemoveDimension(Vector3 value)
        {
            return new Vector2(value.x, value.y);
        }
    }
}


