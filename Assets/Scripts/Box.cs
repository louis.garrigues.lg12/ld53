using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum BoxType { Normal, Player1, Player2 }

public class Box : MonoBehaviour
{
	public static List<Box> list = new List<Box>();

	public Action DoAction;

	[SerializeField] private TMFunds.Audio.AudioPlayer collisionSFX;

	private Vector3 _Velocity;
	[SerializeField] private BoxParameters _Parameters;
	private float _ThrowingTimer;

	[SerializeField] private Rigidbody _Rigidbody;

	[SerializeField] private BoxType _Type;
	public BoxType Type => _Type;

	private Collider col;
	private Player currentPlayer;

	[SerializeField] private MeshRenderer[] lightingParts;
	private Material[] lightingMats;

	[SerializeField] private ParticleSystem _ExplosionEffect;

	// Start is called before the first frame update
	void Start()
	{
		list.Add(this);
		col = GetComponent<Collider>();
		SetModeVoid();

		//Lighting
		lightingMats = new Material[lightingParts.Length];
		for(int i = lightingMats.Length - 1; i >= 0; i--)
        {
			lightingMats[i] = lightingParts[i].material;
        }
	}

    private void OnDestroy()
    {
		list.Remove(this);
    }

    public void SetModeVoid() { DoAction = DoActionVoid; }

    private void DoActionVoid() { }

	public void SetModeReleased(Vector3 pVelocity) 
	{
		currentPlayer = null;
		DoAction = DoActionReleased;

		_Rigidbody.isKinematic = false;
		col.enabled = true;
		transform.parent = null;

		_Rigidbody.velocity = pVelocity;
		_Rigidbody.angularVelocity = new Vector3(pVelocity.magnitude * 0.2f, 0, 0);
		//_Velocity = ;

		//_ThrowingTimer = 0.0f;
	}

	private void DoActionReleased()
	{
		//_ThrowingTimer += Time.deltaTime;

		//if (_ThrowingTimer > _Parameters.ThrowingDuration) SetModeVoid();

		//transform.position += _Velocity * _Parameters.ThrowingSpeed * Time.deltaTime;
	}

	public void SetModeCatch(Transform pParent, Vector3 pPosition, Quaternion pRotation, Player pPlayer) 
	{
		currentPlayer?.LoseBox();
		currentPlayer = pPlayer;
		DoAction = DoActionCatch;

		transform.position = pPosition;
		transform.rotation = pRotation;
		_Rigidbody.isKinematic = true;
		col.enabled = false;
		transform.SetParent(pParent);
	}

	private void DoActionCatch()
	{ 

	}

	void Update()
	{
		DoAction?.Invoke();
	}

    private void OnCollisionEnter(Collision collision)
    {
		float lMagnitude = _Rigidbody.velocity.magnitude;

		Player lPlayer = collision.collider.GetComponent<Player>();
		if (lMagnitude > 4f && lPlayer != null && Vector3.Dot(_Rigidbody.velocity.normalized, (lPlayer.transform.position - transform.position).normalized) > 0f)
		{
			lPlayer.Hit(this, lMagnitude, collision.GetContact(0).point);
        }

        //Sound
        if (soundOk)
        {
			StartCoroutine(waitSound());
			collisionSFX.SetVolume(Mathf.Min(lMagnitude * 0.04f, 0.4f));
			collisionSFX.Play();
		}
    }

	public void Light()
    {
		for(int i = lightingMats.Length - 1; i >= 0; i--)
        {
			lightingMats[i].SetFloat("_Light", 1f);
        }
    }

	public void Unlight()
    {
		for (int i = lightingMats.Length - 1; i >= 0; i--)
		{
			lightingMats[i].SetFloat("_Light", 0f);
		}
	}

	public void Dismantle()
    {
		//currentPlayer?.LoseBox();

		_ExplosionEffect.Play();
		_ExplosionEffect.transform.parent = null;

		transform.DOScale(Vector3.zero, 0.4f).OnComplete(() => 
		{
			Destroy(gameObject); 
		} );
    }

	private bool soundOk = true;
	private IEnumerator waitSound()
    {
		soundOk = false;
		yield return new WaitForSeconds(0.2f);
		soundOk = true;
    }
}