using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMFunds.Patterns;

public class BoxGrabber : MonoBehaviour
{
	//Box grabbing
	private List<Box> grabbableBoxes = new List<Box>();
	[SerializeField] private LayerMask mask;
	[SerializeField] private float length;
	private float minDot = 0.3f;

	private StateMachine loop = new StateMachine();
	private Box current = null;
	private BoxInstruction currentInstr = null;
	private int controllerType = 0;

    private void Start()
    {
		StartCheck();
    }

    private void Update()
    {
		loop.Call();
    }

	public void SetControllerType(int pType)
    {
		controllerType = pType;
    }

	private void CheckingLoop()
    {
		Box lNew = Check();

		if (lNew != current)
		{
			if(current != null)
            {
				current.Unlight();
				currentInstr?.Close();
            }

			if(lNew != null)
            {
				lNew.Light();
				currentInstr = BoxInstructionCanvas.Instance?.CreateInstruction(lNew.transform, controllerType);
			}

			current = lNew;
		}
	}

	public void Emptying()
    {
		current?.Unlight();
		currentInstr?.Close();
		current = null;
    }

	public void StartCheck()
    {
		loop.Set(CheckingLoop);
    }

	public void EndCheck()
    {
		loop.SetVoid();
    }

	public Box Catch()
    {
		return current;
    }

    private Box Check()
    {
		Vector3 lDirTo;
		float lCurrentDot = 0;
		float lWinnerDot = minDot;
		float lNorm;
		Box lCurrent;
		Box lWinner = null;
		for(int i = Box.list.Count - 1; i >= 0; i--)
        {
			lCurrent = Box.list[i];
			lDirTo = lCurrent.transform.position - transform.position;
			lNorm = lDirTo.magnitude;

			if (lNorm > length) continue;

			lDirTo /= lNorm;
			lCurrentDot = Vector3.Dot(transform.forward, lDirTo);
			if (lCurrentDot <= minDot) continue;

			if (Physics.Raycast(transform.position, lDirTo, lNorm, mask)) continue;

			
			if (lCurrentDot > lWinnerDot)
			{
				lWinner = lCurrent;
				lWinnerDot = lCurrentDot;
			}

		}

		return lWinner;
    }
}
