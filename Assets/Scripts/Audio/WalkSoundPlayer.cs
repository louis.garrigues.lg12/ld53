using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMFunds.Audio;

public class WalkSoundPlayer : MonoBehaviour
{
    private Animator animator;
    private AudioPlayer sfx;

    private void Awake()
    {
        sfx = GetComponent<AudioPlayer>();
        animator = GetComponent<Animator>();
    }

    public void Walk()
    {
        sfx.SetVolume(animator.GetFloat("Speed") * 0.1f);
        sfx.SetPitch(Random.Range(0.9f, 1.1f));
        sfx.Play();
    }
}
