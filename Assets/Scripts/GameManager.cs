using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMFunds.Patterns;

public class GameManager : MonoBehaviour
{
    public enum WinCode { Player1, Player2, Draw}

	[SerializeField] private PlayerCanvas _PlayerCanvas;

    [SerializeField] private PlayerSlot _PlayerSlot0;
    [SerializeField] private PlayerSlot _PlayerSlot1;

    [SerializeField] private Player _Player0;
    [SerializeField] private Player _Player1;

    [SerializeField] private DeliveryZone _DZ0;
    [SerializeField] private DeliveryZone _DZ1;

    [SerializeField] private GameObject _Level;


    private StateMachine loop = new StateMachine();
    private float gameCount;

    [Header("Game Parameters")]
    [SerializeField] private int _ScoreToWin = 3;
    [SerializeField] private int gameChrono = 120;

    private void Start()
    {
        _Level.SetActive(false);

        PlayerCanvas.OnStart += StartGame;

        _DZ0.OnScoreUp += ScoreUp0;
        _DZ1.OnScoreUp += ScoreUp1;
    }

    private void StartGame()
    {
        _Player0.SetController(_PlayerSlot0.controller);
        _Player1.SetController(_PlayerSlot1.controller);

        _Level.SetActive(true);

        gameCount = gameChrono;
        loop.Set(GameLoop);
    }

    private void OnDestroy()
    {
        PlayerCanvas.OnStart -= StartGame;

        _DZ0.OnScoreUp -= ScoreUp0;
        _DZ1.OnScoreUp -= ScoreUp1;
    }

    public void ScoreUp0()
    {
        HUD.Instance?.SetScore(0, _DZ0.Score);
        if (_DZ0.Score >= _ScoreToWin) EndGame(WinCode.Player1);
    }

    public void ScoreUp1()
    {
        HUD.Instance?.SetScore(1, _DZ1.Score);
        if (_DZ1.Score >= _ScoreToWin) EndGame(WinCode.Player2);
    }

    private void Update()
    {
        loop.Call();
    }

    private void GameLoop()
    {
        gameCount -= Time.deltaTime;
        HUD.Instance?.SetChrono(Mathf.CeilToInt(gameCount));

        if(gameCount < 0f)
        {
            if (_DZ0.Score == _DZ1.Score) EndGame(WinCode.Draw);
            else if (_DZ0.Score > _DZ1.Score) EndGame(WinCode.Player1);
            else EndGame(WinCode.Player2);
        }
    }

    private void EndGame(WinCode pWinner)
    {
        loop.SetVoid();
        Debug.Log("End : " + pWinner.ToString());
    }
}