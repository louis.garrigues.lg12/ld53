using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryZone : MonoBehaviour
{
	public Action OnScoreUp;

	private int _Score = 0;
	public int Score => _Score;

	[SerializeField] private BoxType _Type;

	private void OnTriggerEnter(Collider pCollided)
	{
		Box lBox;

		if (pCollided.TryGetComponent(out lBox))
		{
			if (lBox.Type == _Type)
			{
				++_Score;
				OnScoreUp?.Invoke();
				lBox.Dismantle();
			}
		}
	}
}