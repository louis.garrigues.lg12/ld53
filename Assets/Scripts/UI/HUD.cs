using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUD : MonoBehaviour
{
    private static HUD instance;
    public static HUD Instance => instance;

    [SerializeField] private TMP_Text chronoText;
    [SerializeField] private TMP_Text[] playerScores;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
    }

    public void SetChrono(int pValue)
    {
        chronoText.text = pValue.ToString();
    }

    public void SetScore(int pPlayer, int pScore)
    {
        playerScores[pPlayer].text = pScore.ToString();
    }
}
