using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxInstructionCanvas : MonoBehaviour
{
    private static BoxInstructionCanvas instance;
    public static BoxInstructionCanvas Instance => instance;

    [SerializeField] private BoxInstruction prefab;

    private void Awake()
    {
        if (instance == null) instance = this;
        else Destroy(gameObject);
    }

    public BoxInstruction CreateInstruction(Transform pTarget, int pType)
    {
        //Debug.Log("Canvas");
        BoxInstruction lObj = Instantiate<BoxInstruction>(prefab, transform);
        lObj.Init(pTarget, pType);
        return lObj;
    }
}
