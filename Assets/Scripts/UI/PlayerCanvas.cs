using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerCanvas : MonoBehaviour
{
    public static Action OnStart;

    [SerializeField] private PlayerSlot playerSlot0;
    [SerializeField] private PlayerSlot playerSlot1;
    [SerializeField] private GameObject physicalMenu;
    [SerializeField] private GameObject nextCam;

    private void Awake()
    {
        playerSlot0.num = 1;
        playerSlot1.num = 2;
        Controller.OnNewController += OnControllerConnection;
        Controller.OnControllerLost += OnControllerDisconnection;
        PlayerSlot.OnReady += OnPlayerSlotReady;
    }

    private void OnDestroy()
    {
        Controller.OnNewController -= OnControllerConnection;
        Controller.OnControllerLost -= OnControllerDisconnection;
        PlayerSlot.OnReady -= OnPlayerSlotReady;
    }

    public void EndAnimation()
    {
        OnStart?.Invoke();
        physicalMenu?.SetActive(false);
        nextCam?.SetActive(true);
    }

    private void OnControllerConnection(Controller pController)
    {
        if (!playerSlot0.HasController)//p1 has no controller
        {
            playerSlot0.AssignController(pController);
        }
        else if (!playerSlot1.HasController)//p2 has no controller
        {
            playerSlot1.AssignController(pController);
        }
        else if(pController.DeviceType == Controller.Device.Keyboard)//already 2 pads
        {
            Destroy(pController.gameObject);
        }
        else
        {
            if (playerSlot0.IsKeyboardControlled)//p1 is keyboard
            {
                playerSlot0.AssignController(pController);
            }
            else if (playerSlot1.IsKeyboardControlled)//p2 is keyboard
            {
                playerSlot1.AssignController(pController);
            }
            else//3rd pad
            {
                Destroy(pController.gameObject);
            }
        }
    }

    private void OnPlayerSlotReady()
    {
        if(playerSlot0.IsReady && playerSlot1.IsReady)
        {
            GetComponent<Animator>().SetTrigger("Close");
            //Destroy(gameObject);
        }
    }

    private void OnControllerDisconnection(Controller pController)
    {
        TestDisconnectedSlot(playerSlot0, pController);
        TestDisconnectedSlot(playerSlot1, pController);
    }

    private void TestDisconnectedSlot(PlayerSlot pSlot, Controller pController)
    {
        if (pSlot.CompareController(pController)) pSlot.CleanController();
    }
}
