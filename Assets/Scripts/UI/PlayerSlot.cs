using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class PlayerSlot : MonoBehaviour
{
    public static Action<Controller, int> AnnounceController;

    private const string READY = "Ready";
    private const string NO_CONTROLLER = "No controller";
    private const string NO_PLAYER = "EMPTY";
    private string NOT_READY => "Not ready" + (HasController ? " [Press " + (IsKeyboardControlled? "Space" : "A" ) + "]" : "");
    private string PLAYER => "Player " + num;

    public static Action OnReady;

    [Header("References")]
    [SerializeField] private TMP_Text playerText;
    [SerializeField] private TMP_Text controllerText;
    [SerializeField] private TMP_Text readyText;

    public Controller controller;
    public bool HasController => controller != null;
    public bool IsKeyboardControlled => HasController && controller.DeviceType == Controller.Device.Keyboard;
    private bool ready = false;
    public bool IsReady => ready;

    public int num = 0;

    private void Awake()
    {
        PlayerCanvas.OnStart += Announce;
        CleanController();
    }

    private void OnDestroy()
    {
        PlayerCanvas.OnStart -= Announce;
    }

    public void Announce()
    {
        if (controller != null)
        {
            controller.OnInput -= OnStartInput;
            AnnounceController?.Invoke(controller, num);
        } 
    }

    public void AssignController(Controller pController)
    {
        if (controller != null) Destroy(controller.gameObject);
        controller = pController;
        controller.OnInput += OnStartInput;

        playerText.text = PLAYER;
        controllerText.text = controller.DeviceType.ToString();
        ready = false;
        readyText.text = NOT_READY;
    }

    public void CleanController()
    {
        if(controller != null) Destroy(controller.gameObject);
        controller = null;
        playerText.text = NO_PLAYER;
        controllerText.text = NO_CONTROLLER;
        ready = false;
        readyText.text = NOT_READY;
    }

    public bool CompareController(Controller pController)
    {
        return pController == controller;
    }

    private void OnStartInput()
    {
        ready = !ready;
        if (ready) OnReady?.Invoke();
        readyText.text = ready ? READY : NOT_READY;
    }
}
