using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxInstruction : MonoBehaviour
{
    private Transform target;
    [SerializeField] private Sprite[] sprites;

    public void Init(Transform pTarget, int pType)
    {
        target = pTarget;
        GetComponent<Image>().sprite = sprites[pType];
        transform.position = Camera.main.WorldToScreenPoint(target.position);
    }

    private void Update()
    {
        if(target != null)
        {
            transform.position = Camera.main.WorldToScreenPoint(target.position);
        }
        else
        {
            Close();
        }
        
    }

    public void Close()
    {
        Destroy(gameObject);
    }
}
