using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
	fileName = "Player Parameters",
	menuName = "Scriptable Objects/Player",
	order = 1)]
public class PlayerParameters : ScriptableObject
{
	[SerializeField] private float _Speed = 1.0f;
	[SerializeField] private float _RotationSpeed = 1.0f;

	public float Speed { get => _Speed; private set { _Speed = value; } }

	public float RotationSpeed { get => _RotationSpeed; private set { _RotationSpeed = value; } }
}