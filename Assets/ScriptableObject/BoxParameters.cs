using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
	fileName = "Box Parameters",
	menuName = "Scriptable Objects/Box",
	order = 2)]
public class BoxParameters : ScriptableObject
{
	[SerializeField] private float _ThrowingSpeed = 1.0f;
	[SerializeField] private float _ThrowingDuration = 1.0f;

	public float ThrowingSpeed { get => _ThrowingSpeed; private set { _ThrowingSpeed = value; } }

	public float ThrowingDuration { get => _ThrowingDuration; private set { _ThrowingDuration = value; } }
}